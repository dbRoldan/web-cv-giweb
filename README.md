# Universidad Distrital Francisco José de Caldas
## Facultad Ingenieria
### ACM – GIWEB – FRONT
![UDistrital](public/images/udistrital_esc.png "Universidad Distrital Francisco José de Caldas")
#### Creador:
Daissi Bibiana Gonzalez Roldan   **Codigo:** 20152020108
#### Pagina Web:
Proyecto de pagina Web con Curriculum:
![Web](public/images/web_pant.png "Curriculum")
#### Descripcion:
El **Curriculum Vitae** es un proyecto orientado a describir a quien presenta este proyecto, como proyecto final para **GIWEB.**
#### Herramientas:
* Bootstrap
* HTML5
#### Link:
[Curriculum Web](https://dbroldan.gitlab.io/web-cv-giweb/ " Pagina Web - Curriculum")

#### Licencia:
MIT License
